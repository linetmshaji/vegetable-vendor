package com.app.greenroot;


import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

public class VendorCategoryActivity extends AppCompatActivity {
    private ImageView Root,Fruits,Leafy,Vegetables;
    private Button LogoutBtn, CheckOrdersBtn, ViewProductsBtn,SettingBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_category);


        LogoutBtn = (Button) findViewById(R.id.admin_logout_btn);
        LogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(VendorCategoryActivity.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        CheckOrdersBtn = (Button) findViewById(R.id.check_orders_btn);
        CheckOrdersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(VendorCategoryActivity.this,VendorHomeActivity.class);
                startActivity(intent);
            }
        });

        Root = (ImageView) findViewById(R.id.root);
        Fruits = (ImageView) findViewById(R.id.fruits);
        Leafy = (ImageView) findViewById(R.id.leafy);
        Vegetables = (ImageView) findViewById(R.id.vegetables);





        Root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(VendorCategoryActivity.this, VendorAddNewProduct.class);
                intent.putExtra("category", " Root");
                startActivity(intent);
            }
        });


        Fruits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(VendorCategoryActivity.this, VendorAddNewProduct.class);
                intent.putExtra("category", "Fruits");
                startActivity(intent);
            }
        });


        Leafy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(VendorCategoryActivity.this, VendorAddNewProduct.class);
                intent.putExtra("category", "Leafy");
                startActivity(intent);
            }
        });


        Vegetables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(VendorCategoryActivity.this, VendorAddNewProduct.class);
                intent.putExtra("category", "Other Vegetables");
                startActivity(intent);
            }
        });
    }
}
