package com.app.greenroot;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.greenroot.Prevalent.VendorPrevalent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class VendorManageProductDetailsActivity extends AppCompatActivity {

    private Button UpdateProductButton, DeleteProductButton;
    private EditText name,description,price;
    private ImageView imageview;
    private String productID="";
    private DatabaseReference productsRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_product_activity);

        productID = getIntent().getStringExtra("pid");
        productsRef = FirebaseDatabase.getInstance().getReference().child("Products").child(productID);

        UpdateProductButton = (Button) findViewById(R.id.update_btn);
        DeleteProductButton = (Button) findViewById(R.id.delete_btn);

        imageview = findViewById(R.id.select_product_image);
        name = findViewById(R.id.product_name);
        description = findViewById(R.id.product_description);
        price = findViewById(R.id.product_price);

        displayProductInfo();
        UpdateProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyChanges();
            }
        });
        DeleteProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct();
            }
        });
    }
    private void deleteProduct(){
        productsRef.removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(VendorManageProductDetailsActivity.this,"Item removed Successfully.",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(VendorManageProductDetailsActivity.this, VendorHomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
    }

    private void applyChanges(){
        String pDescription = description.getText().toString();
        String pPrice = price.getText().toString();
        String pName = name.getText().toString();
            if (pName.equals(""))
            {
            Toast.makeText(this, "Product Name is mandatory...", Toast.LENGTH_SHORT).show();
            }
            else if (pDescription.equals(""))
            {
                Toast.makeText(this, "Please write product description...", Toast.LENGTH_SHORT).show();
            }
            else if (pPrice.equals(""))
            {
                Toast.makeText(this, "Please write product Price...", Toast.LENGTH_SHORT).show();
            }
            else {
                HashMap<String, Object> productMap = new HashMap<>();
                productMap.put("pid", productID);
                productMap.put("description", pDescription);
                productMap.put("price", pPrice);
                productMap.put("pname", pName);
                productsRef.updateChildren(productMap)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(VendorManageProductDetailsActivity.this, "Product is updated successfully..", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(VendorManageProductDetailsActivity.this, VendorHomeActivity.class);
                                    startActivity(intent);
                                    finish();

                                }

                         }
                        });

            }

        }
            private void displayProductInfo() {
                   productsRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            String pName = dataSnapshot.child("pname").getValue().toString();
                            String pDescription = dataSnapshot.child("description").getValue().toString();
                            String pPrice = dataSnapshot.child("price").getValue().toString();

                            name.setText(pName);
                            price.setText(pPrice);
                            description.setText(pDescription);


                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }



}