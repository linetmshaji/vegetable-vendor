package com.app.greenroot;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class CustomerViewCategory extends AppCompatActivity {
    private ImageView Root,Fruits,Leafy,Vegetables;
    private Button LogoutBtn, CheckOrdersBtn, ViewProductsBtn,SettingBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);


        Root = (ImageView) findViewById(R.id.root);
        Fruits = (ImageView) findViewById(R.id.fruits);
        Leafy = (ImageView) findViewById(R.id.leafy);
        Vegetables = (ImageView) findViewById(R.id.vegetables);





        Root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CustomerViewCategory.this, ViewProductDetailsbyCategory.class);
                intent.putExtra("category", " Root");
                startActivity(intent);
            }
        });


        Fruits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CustomerViewCategory.this,ViewProductDetailsbyCategory.class);
                intent.putExtra("category", "Fruits");
                startActivity(intent);
            }
        });


        Leafy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CustomerViewCategory.this,ViewProductDetailsbyCategory.class);
                intent.putExtra("category", "Leafy");
                startActivity(intent);
            }
        });


        Vegetables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(CustomerViewCategory.this, ViewProductDetailsbyCategory.class);
                intent.putExtra("category", "Other Vegetables");
                startActivity(intent);
            }
        });
    }
}
